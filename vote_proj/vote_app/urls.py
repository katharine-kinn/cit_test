from django.urls import path, include
from django.contrib.auth import views as auth_views

from vote_app.api.views import MainView


urlpatterns = [
    path('', MainView.as_view(), name='main'),
    path('accounts/', include('vote_app.api.accounts.urls')),
    path('articles/', include('vote_app.api.articles.urls')),
    path('polls/', include('vote_app.api.polls.urls')),

]
