from django.db import models
from django.urls import reverse


class Article(models.Model):
    title = models.CharField(max_length=255)
    body = models.TextField()

    def get_absolute_url(self):
        return reverse('article-detail', args=[self.id])


class Poll(models.Model):
    title = models.CharField(max_length=255)
    article = models.ForeignKey(Article, on_delete=models.CASCADE, related_name="polls")


class PollQuestion(models.Model):
    question_text = models.TextField()
    poll = models.ForeignKey(Poll, on_delete=models.CASCADE, related_name="questions")


class PollQuestionAnswer(models.Model):
    answer_text = models.TextField()
    question = models.ForeignKey(PollQuestion, on_delete=models.CASCADE, related_name="answers")


class PollResult(models.Model):
    """Answer already contains reference to question
    """
    answer = models.ForeignKey(PollQuestionAnswer, on_delete=models.CASCADE, related_name="results")