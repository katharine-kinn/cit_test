from django.views import View
from django.views.generic import TemplateView


class MainView(TemplateView):
    template_name = 'index.html'


class ParentIdMixin(View):
    """
        Mixin class which gives model view access to a parent object,
        referenced by foreign key.
        Should be used when url contains parent id, for example:
            /article/1/polls/add

    """
    parent_id = None
    parent_id_name = None

    def dispatch(self, request, *args, **kwargs):
        if self.parent_id_name:
            self.parent_id = kwargs.get(self.parent_id_name)
        return super(ParentIdMixin, self).dispatch(request, *args, **kwargs)

    def get_parent_id(self):
        return self.parent_id