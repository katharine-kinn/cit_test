import json

from django.views import View
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import reverse, render
from django.core.exceptions import PermissionDenied
from django.db.models import Count, Sum, F, Q

from vote_app.models import Poll, PollQuestion, PollResult
from vote_app.api.views import ParentIdMixin

from .forms import PollForm
# from .question_forms import PollVoteFormset


__all__ = ['Polls', 'PollDetail', 'PollCreate', 'PollVote', 'PollResults']


class Polls(ListView):
    queryset = Poll.objects.all().select_related('article')
    template_name = 'polls/list.html'
    context_object_name = 'polls'


class PollDetail(DetailView):
    model = Poll
    template_name = 'polls/detail.html'
    queryset = Poll.objects.prefetch_related('questions')


class PollCreate(ParentIdMixin, LoginRequiredMixin, CreateView):
    form_class = PollForm
    template_name = 'polls/create.html'
    parent_id_name = 'article_id'

    def get_context_data(self, **kwargs):
        context = super(PollCreate, self).get_context_data(**kwargs)
        context['article_id'] = self.get_parent_id()
        return context

    def get_success_url(self):
        return reverse('poll-detail', args=[self.object.id])

    def form_valid(self, form):
        poll = form.save(commit=False)
        poll.article_id = self.get_parent_id()
        poll.save()
        self.object = poll
        return HttpResponseRedirect(self.get_success_url())


class PollVote(View):
    """
        Receives vote results as ajax payload
        and creates poll results.

        Returns dict {
            'poll_id': poll_id,
            'data': [
                {
                    'question_id': question_id,
                    'answer_id': answer_id
                }
            ]
        }

        If payload data is invalid, returns error message.
    """

    def post(self, request, *args, **kwargs):
        if request.is_ajax:
            result = {'poll_id': kwargs['poll_id'], 'data': []}
            error = {'error': None}
            payload = dict(request.POST)

            # if payload contains invalid literals
            try:
                payload_safe = {int(k): [int(v) for v in payload[k]] for k in payload.keys()}
            except ValueError(e):
                error['error'] = {
                        'msg': 'Неверные значения ответов'
                    }
                return JsonResponse(error)

            questions = PollQuestion.objects.filter(id__in=payload_safe.keys()).prefetch_related('answers')
            questions_dict = {x.id: x for x in questions}

            for qid, answers in payload_safe.items():
                db_answers = (x.id for x in questions_dict[qid].answers.all())

                if not (set(answers) & set(db_answers)): # check if ids received from client are invalid
                    error['error'] = {
                            'question_id': qid,
                            'msg': 'Не существует данных ответов'
                        }
                    return JsonResponse(error)

                for answer in answers:
                    poll_result = PollResult.objects.create(answer_id=answer)
                    poll_result.save()
                    result['data'].append({
                        'question_id': qid,
                        'answer': answer
                    })

            return JsonResponse(result)

        raise PermissionDenied()


class PollResults(DetailView):
    model = Poll
    template_name = 'polls/results.html'

    def get_answer_percents(self, poll_results):
        """
            Returns dict 
            {
                <question_id>: {
                    "text": <question_text>,
                    "values": [
                        (<answer_text>, <votes for answer>/<total_votes_for_question>)
                    ]
                }
            }
        """
        total_count_qs = poll_results\
            .values('answer__question', 'answer__question__answers') \
            .annotate(count=Count('answer__question__answers')) \
            .values('answer__question', 'count') \
            .distinct()

        count_qs = poll_results \
            .values('answer__question',
                'answer',
                'answer__answer_text',
                'answer__question__question_text'
            ) \
            .annotate(answer_count=Count('answer'))

        total_count_dict = {v['answer__question']: v['count'] for v in total_count_qs}

        percent_dict = {v['answer__question']: {'text': None, 'values': []} for v in count_qs}
        for count in count_qs:
            key = count['answer__question']
            total = total_count_dict[key]
            count_per_answer = count['answer_count']
            percent = float(count_per_answer) / float(total)
            percent_dict[key]['text'] = count['answer__question__question_text']
            percent_dict[key]['values'].append((count['answer__answer_text'], percent))

        return percent_dict

    def get_context_data(self, **kwargs):
        context = super(PollResults, self).get_context_data(**kwargs)

        poll_results = PollResult.objects.filter(answer__question__poll=self.object) \
            .prefetch_related('answer__question','answer__question__answers')

        percents = self.get_answer_percents(poll_results)

        prepared_percents = []
        for qid, data in percents.items():
            prepared_answer = [[], []]
            for answer_data in data['values']:
                prepared_answer[0].append(answer_data[0])
                prepared_answer[1].append(answer_data[1])
            prepared_percents.append(
                (qid,
                    data['text'],
                    (
                        json.dumps(prepared_answer[0]), 
                        json.dumps(prepared_answer[1])
                    )
                )
            )

        context['poll_results'] = prepared_percents
        return context

