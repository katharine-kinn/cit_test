from django import forms

from vote_app.models import Poll


class PollForm(forms.ModelForm):

    class Meta:
        model = Poll
        fields = ['title']

