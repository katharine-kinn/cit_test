from django import forms

from vote_app.models import PollQuestion, PollQuestionAnswer


class PollQuestionForm(forms.ModelForm):

    class Meta:
        model = PollQuestion
        fields = ['question_text']


PollAnswerFormset = forms.inlineformset_factory(PollQuestion, PollQuestionAnswer, fields=('answer_text', ))


# class PollVoteForm(forms.Form):
#     question_id = forms.CharField(disabled=True)

#     def __init__(self, *args, **kwargs):
#         super(PollVoteForm, self).__init__(*args, **kwargs)
#         if 'initial' in kwargs:
#             question_id = kwargs['initial']['question_id']
#             answer_choices = ((q.id ,i) for i, q in enumerate(PollQuestionAnswer.objects.filter(question_id=question_id)))
#             self.fields['answers'] = forms.ChoiceField(choices=answer_choices, required=False)


# PollVoteFormset = forms.formset_factory(PollVoteForm, extra=0)


