from django.urls import path

from .views import *
from .question_views import *


urlpatterns = [
    path('', Polls.as_view(), name='poll-list'),
    path('<int:pk>/', PollDetail.as_view(), name='poll-detail'),
    path('<int:pk>/results', PollResults.as_view(), name='poll-results'),

    path('<int:poll_id>/vote', PollVote.as_view(), name='poll-vote'),
    path('<int:poll_id>/questions/add', PollQuestionCreate.as_view(), name='poll-question-create'),

]
