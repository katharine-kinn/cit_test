from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import reverse

from vote_app.models import (
    Poll,
    PollQuestion,
    PollQuestionAnswer
)

from vote_app.api.views import ParentIdMixin

from .question_forms import (
    PollQuestionForm,
    PollAnswerFormset
)


__all__ = ['PollQuestionCreate']


class PollQuestionCreate(ParentIdMixin, LoginRequiredMixin, CreateView):
    model = PollQuestion
    fields = ['question_text']
    template_name = 'polls/create_question.html'
    parent_id_name = 'poll_id'

    def get_context_data(self, **kwargs):
        context = super(PollQuestionCreate, self).get_context_data(**kwargs)
        context['poll_id'] = self.get_parent_id()
        context['form'] = PollQuestionForm()
        context['formset'] = PollAnswerFormset()
        return context

    def get_success_url(self):
        return reverse('poll-detail', args=[self.get_parent_id()])

    def post(self, request, *args, **kwargs):
        form = PollQuestionForm(data=request.POST)
        formset = PollAnswerFormset(request.POST)
        if form.is_valid() and formset.is_valid():
            return self.form_valid(formset, form)
        
        return self.form_invalid(form)

    def form_valid(self, formset, form):
        question = form.save(commit=False)
        question.poll_id = self.get_parent_id()
        question.save()
        self.object = question

        formset.instance = self.object
        formset.save()

        return HttpResponseRedirect(self.get_success_url())

