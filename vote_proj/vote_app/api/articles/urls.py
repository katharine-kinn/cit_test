from django.urls import path

from .views import *
from vote_app.api.polls.views import PollCreate

urlpatterns = [
    path('', ArticleList.as_view(), name='article-list'),
    path('<int:pk>/', ArticleDetail.as_view(), name='article-detail'),
    path('add/', ArticleCreate.as_view(), name='article-create'),

    path('<int:article_id>/polls/add', PollCreate.as_view(), name='article-poll-create')
]
