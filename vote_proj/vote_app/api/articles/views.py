"""API:
    article/add
    article/<id>
"""

from django.views.generic import (ListView, DetailView)
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin

from vote_app.models import Article


__all__ = ['ArticleList', 'ArticleDetail', 'ArticleCreate']


class ArticleList(ListView):
    model = Article
    template_name = 'articles/list.html'
    context_object_name = 'articles'


class ArticleDetail(DetailView):
    model = Article
    template_name = 'articles/detail.html'


class ArticleCreate(LoginRequiredMixin, CreateView):
    model = Article
    fields = ['title', 'body']
    template_name = 'articles/create_form.html'



