$(document).ready(function() {

    var selectedVariants = {};

    $(".answer-variant").click(function(e){
        var questionId = $(this).closest(".question").attr("question-id");
        var answerId = $(this).attr("answer-id");
        selectedVariants[questionId] = answerId;

        var selectedClassName = 'answer-variant-selected';

        var affectedElement = $(this).children("div").first();

        if (affectedElement != undefined) {
            if (affectedElement.hasClass(selectedClassName)) {
                affectedElement.removeClass(selectedClassName);
                delete selectedVariants[questionId]
            } else {
                $(this).closest(".answer").find("div").removeClass(selectedClassName);
                affectedElement.addClass(selectedClassName);
            }
        }   

    });

    function validate_vote_form() {
        var errors = []
        var selectedVariantsLength = Object.keys(selectedVariants).length
        if ($(".question").length != selectedVariantsLength) {
            $(".question").each(function(){
                if (!($(this).attr("question-id") in selectedVariants)) {
                    $(this).addClass("error-empty-answer");
                }
            });
            errors.push("Пожалуйста, ответьте на все вопросы");
        }
        return errors
    }

    var dialogSuccess = $("#dialog-success").dialog({
        modal: true,
        autoOpen: false,
        buttons: {
            Ok: function() {
                $(this).dialog("close");
            }
        }
    });

    var dialogFail = $("#dialog-fail").dialog({
        modal: true,
        autoOpen: false,
        buttons: {
            Ok: function() {
                $(this).dialog("close");
            }
        }
    });

    $("#vote-form").submit(function(e){
        e.preventDefault();

        var errors = validate_vote_form(); 
        if (errors.length == 0) {

            var xhr = $.post($(this).attr('action'), selectedVariants);

            xhr.done(function(data) {
                    if ('error' in data) {
                        $("#dialog-fail-error-message").text(data['error']['msg']);
                        dialogFail.dialog("open");
                    } else {
                        dialogSuccess.dialog("open");
                    }
                    
                })
                .fail(function(data) {
                    console.log(data)
                });
        }

        return false;
    });

});