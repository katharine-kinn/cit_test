$(document).ready(function(){

    $('.vote-chart').each(function(){
        var ctx = $(this);

        var labels = JSON.parse(JSON.parse('"' + ctx.attr('labels') + '"'));
        var data = JSON.parse(JSON.parse('"' + ctx.attr('data') + '"'));

        var chart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: labels,
                datasets: [{
                    label: '',
                    data: data,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });  
    })



});